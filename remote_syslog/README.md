# Remote Syslog Monitor

This is an *example* Monitor extension to enable GUARDARA to receive logs from Syslog for analysis. 

## 0. SDK Setup

Make sure you have a developer account registered in GUARDARA under the *Settings / Developers* page, and your SDK is configured before moving to the next steps.

## 1. Packaging

Just outside of the root directory of the extension, issue the following SDK command:

```bash
guardara extension -e monitor -o package -n remote_syslog
```

## 2. Deployment

The packaging process generates a deployable package with the file name `g-monitor-remote_syslog.g` in the current directory. Upload the generated package on the *Inventory* tab of the *Settings* page.

## 3. Usage

Once the monitor extension is deployed, you can add the monitor to the Target(s) defined for your project. 

To test the extension:

 1. Add a regular expression, for example,`.*failure.*`, to the *Patterns* configuration option. Leave all the other options default.
 2. Save the Monitor configuration.
 3. Save the Project.
 4. Update your Flow Template actions to pause the job when a Monitor reports an issue.
 4. Start the test.
 5. Run the command below from the terminal.
 
```bash
echo "Simulating a failure" | nc ${ENGINE_ADDRESS} 9514
```

In response to the above command, you should find your test in a paused state with a finding.
