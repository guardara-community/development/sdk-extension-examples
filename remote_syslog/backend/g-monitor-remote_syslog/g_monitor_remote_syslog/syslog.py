import socket
from copy import deepcopy
from threading import Event, Thread
from g_monitor_remote_syslog.processor import Processor
from traceback import format_exc


class Syslog(Thread):
    def __init__(
        self, request, response, protocol, port, patterns, source, debug=False
    ):
        Thread.__init__(self)
        self.request = request
        self.response = response
        self.protocol = protocol
        self.port = port
        self.patterns = patterns
        self.source = source
        self.debug = debug
        self.read_buffer = b""
        self.should_stop = Event()

    # -------------------------------------------------------------------------
    # Public Methods
    # -------------------------------------------------------------------------

    def stop(self):
        self.should_stop.set()

    def run(self):
        try:
            self.__prepare_socket()
            self.socket.bind(("0.0.0.0", self.port))
            handler = self.__run_udp
            if self.protocol == "TCP":
                handler = self.__run_tcp
            self.__set("ready", True)
            handler(self.source, self.patterns)
        except Exception as ex:
            print(f"Remote syslog monitor failed: {str(ex)}.")
            if self.debug:
                print(format_exc())
            self.response.put({"command": "exception", "exception": ex})
        finally:
            print(f"Stopped remote syslog listener processor.")
            self.__set("ready", False)
            self.__cleanup()

    # -------------------------------------------------------------------------
    # Private Methods
    # -------------------------------------------------------------------------

    def __set(self, key, value):
        self.response.put({"command": "set", "key": key, "value": value})

    def __prepare_socket(self):
        if self.protocol == "TCP":
            self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            return
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

    def __cleanup(self):
        if self.socket is None:
            return
        self.socket.close()
        self.socket = None

    def __read_line(self, sock):
        data = b""
        if len(self.read_buffer) > 0:
            newline_pos = self.read_buffer.find(b"\n")
            if newline_pos != -1:
                data += deepcopy(self.read_buffer[0:newline_pos])
                self.read_buffer = self.read_buffer[newline_pos + 1 :]
                return data

        data += deepcopy(self.read_buffer)
        self.read_buffer = b""

        sock.settimeout(0.5)
        while True and not self.should_stop.is_set():
            blist = b""

            try:
                blist = sock.recv(1024)
                if len(blist) == 0:
                    return None
            except socket.timeout:
                continue
            except socket.error:
                return None

            blist = blist.replace(b"\r\n", b"\n")
            newline_pos = blist.find(b"\n")
            if newline_pos == -1:
                data += blist
                continue
            data += blist[0:newline_pos]
            self.read_buffer = blist[newline_pos + 1 :]
            return data

    def __source_allowed(self, source, client_addr):
        if not isinstance(source, str) or len(source) == 0:
            return True
        if source == client_addr:
            return True
        return False

    def __run_tcp(self, source, patterns):
        self.socket.listen(1)
        self.socket.settimeout(0.1)
        print(f"Started remote syslog listener on port {self.port}/tcp.")
        while not self.should_stop.is_set():
            conn = None
            addr = None
            try:
                conn, addr = self.socket.accept()
            except socket.timeout:
                continue
            if self.__source_allowed(source, addr[0]) is not True:
                conn.close()
                break
            while not self.should_stop.is_set():
                line = None
                self.c_action = None
                line = self.__read_line(conn)
                if line is None:
                    try:
                        conn.close()
                    except:
                        pass
                    break
                result = Processor.check_match(patterns, line)
                if result is not True:
                    continue
                finding = Processor.prepare_finding(line)
                self.__set("state", finding)

    def __run_udp(self, source, patterns):
        self.socket.settimeout(0.5)
        print(f"Started remote syslog listener on port {self.port}/udp.")
        while not self.should_stop.is_set():
            line = None
            addr = None
            try:
                line, addr = self.socket.recvfrom(65535)
            except socket.timeout:
                continue
            if (
                not isinstance(line, bytes)
                or self.__source_allowed(source, addr[0]) is not True
            ):
                continue
            result = Processor.check_match(patterns, line)
            if result is not True:
                continue
            finding = Processor.prepare_finding(line)
            self.__set("state", finding)
