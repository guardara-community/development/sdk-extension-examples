from queue import Queue, Empty
from copy import deepcopy
from guardara.sdk.monitor.monitor import MonitorInterface

from g_monitor_remote_syslog.syslog import Syslog


class monitor(MonitorInterface):
    def __init__(self, properties: dict, request_queue, response_queue):
        MonitorInterface.__init__(self, properties, request_queue, response_queue)
        self.handler_request_queue = Queue()
        self.handler_response_queue = Queue()
        self["ready"] = False
        self["state"] = None
        self.handler = Syslog(
            self.handler_request_queue,
            self.handler_response_queue,
            self.get("protocol"),
            self.get("port"),
            self.get("patterns"),
            self.get("source"),
            self.get("debug"),
        )

    def do_start(self) -> bool:
        self.handler.start()
        return True

    def do_stop(self) -> bool:
        self.handler.stop()
        self.handler.join()
        return True

    def do_is_ready(self) -> bool:
        return self.get("ready")

    def do_status(self):
        report = deepcopy(self.get("state"))
        self["state"] = None
        return report

    def custom_loop(self):
        response = self.__get_handler_response()
        if not isinstance(response, dict) or response.get("command") is None:
            return
        if response.get("command") == "exception":
            self["ready"] = False
            raise response.get("exception")
        if response.get("command") == "set":
            self[response.get("key")] = response.get("value")

    def __get_handler_response(self):
        try:
            response = self.handler_response_queue.get(timeout=0.01)
            self.handler_response_queue.task_done()
            return response
        except Empty:
            return None
