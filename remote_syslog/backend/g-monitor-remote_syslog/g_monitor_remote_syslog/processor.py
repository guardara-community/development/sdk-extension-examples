import re


class Processor:
    @staticmethod
    def ensure_str(value):
        return value.decode("latin-1") if isinstance(value, bytes) else value

    @staticmethod
    def check_match(patterns, line):
        line = Processor.ensure_str(line)
        for pattern in patterns.replace("\r\n", "\n").split("\n"):
            if re.match(pattern, line) is not None:
                return True
        return False

    @staticmethod
    def prepare_finding(line):
        if isinstance(line, bytes):
            line = line.decode("latin-1")
        return f"The following log entry matched one of the detection patterns configured:\n\n{line}\n"
