import struct

from guardara.sdk.transform.transform import TransformInterface


class transform(TransformInterface):

    @staticmethod
    def transform(value: bytes, params=None) -> bytes:
        """
        Transform entry point.
        """
        value = transform.ensure_zero(value, params.get('index'))
        checksum = transform.calculate_checksum(value)
        checksum = list(checksum)
        value[params.get('index')] = checksum[0]
        value[params.get('index') + 1] = checksum[1]
        return bytes(value)

    @staticmethod
    def calculate_checksum(value):
        """
        The checksum is the 16-bit one's complement of the one's
        complement sum of the ICMP message starting with the ICMP Type.
        For computing the checksum, the checksum field should be zero.
        If the total length is odd, the received data is padded with 
        one octet of zeros for computing the checksum. This checksum 
        maybe replaced in the future.
        """
        if (len(value) % 2):
            value += "\x00"
        sum = 0
        for i in range(0, len(value), 2):
            a = value[i]
            b = value[i+1]
            sum = sum + (a+(b << 8))
            sum = sum + (sum >> 16)
        sum = ~sum & 0xffff
        return struct.pack("<H", sum)

    @staticmethod
    def ensure_zero(value, index):
        """
        Make sure the bytes at index and index + 1 are NULL before checksum
        calculation.
        """
        value = list(value)
        value[index] = 0
        value[index + 1] = 0
        return value
