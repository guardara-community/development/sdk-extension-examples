import os
import json
from os import path
from codecs import open
from setuptools import setup, find_packages

HERE = path.abspath(path.dirname(__file__))
NAME = "g_transform_icmp_checksum"
MANIFEST_PATH = os.sep.join([HERE, NAME, "manifest.json"])
MANIFEST = json.load(open(MANIFEST_PATH))

with open(path.join(HERE, "README.rst"), encoding="utf-8") as f:
    long_description = f.read()

setup(
    name=NAME,
    version=MANIFEST.get("version"),
    description=MANIFEST.get("description"),
    long_description=long_description,
    url=MANIFEST.get("url"),
    author=MANIFEST.get("author").get("name"),
    author_email=MANIFEST.get("author").get("email"),
    license=MANIFEST.get("license"),
    package_data={"": ["*"]},
    install_requires=[],
    classifiers=[
        "Development Status :: 4 - Beta",
        "Intended Audience :: Developers",
        "Topic :: Software Development :: Testing",
        "License :: OSI Approved :: MIT License",
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: 3.8",
        "Programming Language :: Python :: 3.9",
    ],
    keywords="software development testing QA quality assurance security",
    packages=find_packages(exclude=[]),
    entry_points={
        "console_scripts": [],
    },
)
