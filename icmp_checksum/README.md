# ICMP Checksum Calculator

While GUARDARA makes difficult or time-consuming tasks quick and easy, sometimes, you may not be able to do everything straightforwardly using the built-in components. For example, the ICMP protocol utilizes a checksum value to detect data corruption. In this example, we discuss the challenge of calculating the ICMP checksum and the possible implementation of the checksum calculation in the form of a Transform.

## Explanation

The challenge with ICMP is that the checksum is not at the beginning or end, but it is within the “protected” are of the ICMP header.

The ICMP checksum is calculated by having *NULL* bytes set for the checksum field during the calculation. The resulting checksum is then written to this area. Using the built-in *Hash Field* (assuming it supported IPv4 checksum calculation) to calculate the checksum would result in an error because support fields, such as the *Hash Field*, work on the rendered value of *Group Fields*. If the *Hash Field* was a child of the *Group Field* for which we wanted to calculate the checksum, rendering the *Group Field* would result in recursion. It's a chicken and egg kind of a problem, as the parent Group Field's rendered value depends on the value of the child field's; therefore, we have to find another solution.

As per the above, we use a Transform to implement the checksum calculation. We can apply the transform to the target Group to dynamically calculate the checksum for each test case.

## 0. SDK Setup

Make sure you have a developer account registered in GUARDARA under the *Settings / Developers* page, and your SDK is configured before moving to the next steps.

## 1. Packaging

Just outside of the root directory of the extension, issue the following SDK command:

```bash
guardara extension -e transform -o package -n icmp_checksum
```

## 2. Deployment

The packaging process generates a deployable package with the file name `g-transform-icmp_checksum.g` in the current directory. Upload the generated package on the *Inventory* tab of the *Settings* page.

**IMPORTANT**: We will use the Preview feature of the Message Template designer to see how the transform operates. For this to work, the Engine has to install the extension first. However, the built-in Engine named *Destroyer* is not allowed to install extensions or run tests. Because of this, you will have to shut down the *Destroyer* Engine (`docker stop guardara_destroyer`) and run an external Engine instead. Please refer to the documentation to learn how to set up an external Engine.

## 3. Usage

Once the Transform extension is deployed, you can apply the Transform any Fields within your Message Templates. 

To test the extension:

 1. Create a new Message Template.
 2. Create a new Group Field in the Message Template and add a String Field as the Group Field's children.
 3. Set the default value of the String Field to `AAAAAAAA`.
 4. Edit the Group Field. Close the *Properties* panel and open the *Transforms* panel.
 5. Select the ICMP Checksum transform and click the *ADD* button.
 6. Save the Group Field by clicking the *UPDATE* button.
 7. Click the Preview button in the top-right corner of the Message Template Designer to see what impact the transform had on the default value we set in step #3. You should see `AA<<AAAA` instead of `AAAAAAAA`. The `<<` characters are the ASCII representation of the calculated checksum (`0x3c3c`) starting at index `2`.
