# SDK Extension Examples

Examples on how to create extensions using the GUARDARA SDK.

| Example                          | Type    | Description |
| -------------------------------- | ------- | ----------- |
| [remote_syslog](./remote_syslog/) | Monitor | Example Monitor that acts as a syslog server to receive logs for analysis. |
| [icmp_checksum](./icmp_checksum/) | Transform | Example Transform to calculate ICMP checksums. |
